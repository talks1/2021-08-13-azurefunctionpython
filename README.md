
This is a repo to 
- setup an azure function app
- with an HttpTrigger and an EventHub trigger
- for python code base


![](./images/Pipeline.png)


[This was mainly created from this documentation](https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-function-linux-custom-image?tabs=bash%2Cportal&pivots=programming-language-python)

## Prerequisites

#### Azure Function Core Tools
[Azure Function Core Tools](https://docs.microsoft.com/en-us/azure/azure-functions/functions-run-local?tabs=linux%2Ccsharp%2Cportal%2Cbash%2Ckeda)

#### Docker

- [Docker on ubuntu](https://docs.docker.com/engine/install/ubuntu/)
- [Docker Desktop on Windows](https://www.docker.com/products/docker-desktop)

#### Poetry
```
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python -
```

## Initialize project

[Instructions here](https://python-poetry.org/docs/)
```
poetry new PythonAzureFunction

cd PythonAzureFunction
poetry install
poetry run pytest
```


##  Setup for Azure Functions
[Create Azure Function Docker project](https://docs.microsoft.com/en-us/azure/azure-functions/functions-create-function-linux-custom-image?tabs=bash%2Cportal&pivots=programming-language-python)

```
func init afpython --worker-runtime python --docker
```

##  Create an HTTP Trigger

```
func new --template "Http Trigger" --name HttpTrigger
```

##  Create an EventHub Trigger

```
func new --template "Http Trigger" --name HttpTrigger
```

## Configuration

Update local.settings.json as webjobs storage is required for Web Job
```
{
  "IsEncrypted": false,
  "Values": {
    "FUNCTIONS_WORKER_RUNTIME": "python",
    "EVENTHUB_CONNECTION_STRING":"Endpoint=sb://...",  <-----------
    "AzureWebJobsStorage": "DefaultEndpointsProtocol=https;...", <-----------    
  }
}
```

Update func.json to make http end point public
```
{
  ....
  "bindings": [
    {
      "authLevel": "anonymous", <------ This is updated to anonymous
      "type": "httpTrigger",
      "direction": "in",
      ....
    },   
  ]
}
```



## Running Locally

### Running during develop

```
poetry run func start
````

### Build and run docker image locally

```
docker build --tag afpython .

docker run -p 8080:80 -it 
```

This will start the web server on port 8080. The event hub trigger will run each time an event occurs on the event hub
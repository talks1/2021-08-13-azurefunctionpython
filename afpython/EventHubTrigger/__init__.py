from typing import List
import logging
import os
import azure.functions as func
import json  

import asyncio
from azure.eventhub.aio import EventHubProducerClient
from azure.eventhub import EventData


async def run(connectionString):
    # Create a producer client to send messages to the event hub.
    # Specify a connection string to your event hubs namespace and
    # the event hub name.
    producer = EventHubProducerClient.from_connection_string(conn_str=connectionString, eventhub_name="")
    async with producer:
        # Create a batch.
        event_data_batch = await producer.create_batch()


        response= { "type": "ExtractionResponse",
                    "entity": { 
                    "PROJ_ID": "FooBar",
                    "PROJ_NAME": "St Mary ' S Concept Design",
                    "PROJ_CLNT": "Sydney Water",
                    "POINTS": {
                        "North": "296293.8",
                        "PointID": "SMS _ BH02",
                        "METHOD": {
                        "0": {
                            "Depth": "0.65",
                            "ISPT_NVAL": "21"
                        },
                        "1": {
                            "Depth": "1.71",
                            "ISPT_NVAL": "33"
                        },
                        },
                        "LOCA_LOCA": "St Mary ' S , Nsw",
                        "East": "6265349.1",
                        "HoleDepth": "7.82",
                        "LOCA_STAR": "7/9/2018",
                        "GEOL": {
                        "0": {
                            "Depth": "0.02",
                            "GEOL_BASE": "0.51",
                            "GEOL_DESC": " Silty Gravelly SAND, dark brown, fine to coarse grained, fine, sub-angular gravel, silt is low plasticity (fill)."
                        },
                        "1": {
                            "Depth": "0.51",
                            "GEOL_BASE": "1.01",
                            "GEOL_DESC": " Silty CLAY, pale brown, mottled pale grey, medium plasticity, with fine grained sand, trace fine to medium, sub-rounded ironstone gravels (alluvium)."
                        },                
                        },
                        "LOCA_ENDD": "7/9/2018",
                        "Elevation": ""
                    }
                }
        } 

        # Add events to the batch.
        event_data_batch.add(EventData(json.dumps(response)))
        

        # Send the batch of events to the event hub.
        await producer.send_batch(event_data_batch)


def main(events: List[func.EventHubEvent]):
    for event in events:
        logging.info('Python EventHub trigger processed an event: %s',
                        event.get_body().decode('utf-8'))
        connectionString = os.environ["EVENTHUB_CONNECTION_STRING"]
        logging.info(connectionString) 
        
        evt = event.get_body().decode('utf-8')
        evtDict = json.loads(evt)
        logging.info(evtDict)
        if evtDict['type'] == 'ExtractionRequest':
            logging.info("Processing Request")
            loop = asyncio.new_event_loop()
            loop.run_until_complete(run(connectionString))



